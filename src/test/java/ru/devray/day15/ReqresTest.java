package ru.devray.day15;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;

public class ReqresTest extends AuthorizationTest {

    //RequestSpecification ResponseSpecification
        ResponseSpecification responseSpecification = new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
        RequestSpecBuilder specBuilder = new RequestSpecBuilder();
        RequestSpecification requestSpecification = specBuilder
                .setBaseUri("")
                .log(LogDetail.ALL).build();


    @Test
    public void testR() {

        RestAssured
                .given()
                .spec(requestSpecification)
                .queryParam("user", 2)
                .when()
                .get("https://reqres.in/api/users/{user}")
                .then()
                .spec(responseSpecification);
    }

    @Test
    public void test() {
        //BDD given() when() then()
        RestAssured
                .given()
                .log().all()
                .baseUri("https://reqres.in/api/users/2")
                .queryParam("user", "Alex")
                .when()
                .get() //
                .then()
                .spec(responseSpecification);
        //PathParameters & QueryParameters
        //QP https://reqres.in/api/users?user=ALex&category=sport&price=low
    }

    @Test
    public void testBody() {
        //BDD given() when() then()
        RestAssured
                .given()
                .log().all()
                .baseUri("https://reqres.in/api/users/2")
                .when()
                .get()
                .then()
                .log().all()
                .statusCode(200)
                .header("Content-Type", "application/json; charset=utf-8")
                .body("data.id", equalTo(2)); //JsonPath (Json) - XPath (XML,HTML)

    }

    @Test
    public void testBody1() {
        //BDD given() when() then()
        ResponseBodyExtractionOptions body = RestAssured
                .given()
                .log().all()
                .baseUri("https://reqres.in/api/users/2")
                .when()
                .get()
                .then()
                .log().all()
                .statusCode(200)
                .header("Content-Type", "application/json; charset=utf-8")
                .extract().body();

        String userId = body.jsonPath().get("data.id");
        String userAvatarUrl = body.jsonPath().get("data.avatar");

        //

    }

    @Test
    public void testRandomData() {
//
//        Name expectedName = new Name("Alex","Bob","Wererw");

        Name actualName = RestAssured.given()
                .baseUri("https://randomuser.me/api/")
                .when()
                .get()
                .then()
//                .body("data.title", equalTo(2))
//                .body("data.title", equalTo(2))
//                .body("data.title", equalTo(2))
                .extract().body().jsonPath().getObject("results[0].name", Name.class);

//        Assert.assertEquals(expectedName, actualName);
    }

}
