package ru.devray.day15;


//POJO plain old java object
//DTO

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Name{
    public String title;
    public String first;
    public String last;
    public String id;
}